
function ensurePermission(permissions) {
    return new Promise((resolve, reject) => {
        cordova.exec(resolve, reject, "SMSReader", "permission", [permissions]);
    });
}

function fetchSms(action, selectionQuery, sortQuery, permissions) {

    return ensurePermission(permissions)
        .then((success) => {
            return new Promise((resolve, reject) => {
                cordova.exec(resolve, reject, "SMSReader", action, [selectionQuery, sortQuery]);
            });
        }, (err) => {
            return Promise.reject(err);
        });
}

module.exports = {
    getAllSMS: function (selectionQuery = null, sortQuery = null) {
        return fetchSms("all", selectionQuery, sortQuery, ['read']);
    },
};
